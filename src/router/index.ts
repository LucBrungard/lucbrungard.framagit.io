// Composables
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/Layout.vue'),
    children: [
      {
        path: '',
        redirect: 'accueil'
      },
      {
        path: 'accueil',
        name: 'accueil',
        component: () => import('@/views/Accueil.vue'),
      },
      {
        path: 'actualite',
        name: 'actualite',
        component: () => import('@/views/Actualite.vue'),
      },
      {
        path: 'bug',
        name: 'bug',
        component: () => import('@/views/Bug.vue'),
      },
      {
        path: 'contact',
        name: 'contact',
        component: () => import('@/views/Contact.vue'),
      },
      {
        path: 'contribuer',
        name: 'contribuer',
        component: () => import('@/views/Contribuer.vue'),
      },
      {
        path: 'faq',
        name: 'faq',
        component: () => import('@/views/Faq.vue'),
      },
      {
        path: 'manuel',
        name: 'manuel',
        component: () => import('@/views/Manuel.vue'),
      },
      {
        path: 'telechargement',
        name: 'telechargement',
        component: () => import('@/views/Telechargement.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
